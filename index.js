const express = require("express");

const https = require("https");
const request = require("axios");
const qs = require("querystring");
const config = require("./Paytm/config");
const checksum_lib = require("./Paytm/checksum");
const checksum_lib2 = require("./Paytm/checksum2");

const app = express();

const parseUrl = express.urlencoded({
    extended: false
});
const parseJson = express.json({
    extended: false
});

const PORT = process.env.PORT || 4000;

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

app.get("/status", (req, res) => {
    res.sendFile(__dirname + "/status.html");
});

app.get("/refund", (req, res) => {
    res.sendFile(__dirname + "/refund.html");
});

app.get("/refundStatus", (req, res) => {
    res.sendFile(__dirname + "/refund_status.html");
});

app.get("/processTransaction", (req, res) => {
    res.sendFile(__dirname + "/processTransaction.html");
});

app.post("/processTransaction", [parseUrl, parseJson], async (req, res) => {

    let orderId = 'TEST_'  + new Date().getTime();
    req.body.orderId = orderId;
    let txtToken = await initiateTransaction(req.body);

    var paytmParams = {};

    paytmParams.body = {
        "requestType" : "NATIVE",
        "mid"         : config.PaytmConfig.mid,
        "orderId"     : req.body.orderId,
        "paymentMode" : req.body.paymentMode,
        "channelCode" : req.body.channelCode
    };  

    karza


    paytmParams.head = {
        "txnToken"    : txtToken
    };

    var post_data = JSON.stringify(paytmParams);

    var options = {

        /* for Staging */
        hostname: 'securegw-stage.paytm.in',

        /* for Production */
        // hostname: 'securegw.paytm.in',

        port: 443,
        path: `/theia/api/v1/processTransaction?mid=${config.PaytmConfig.mid}&orderId=${orderId}`,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': post_data.length
        }
    };

    var response = "";
    var post_req = https.request(options, function(post_res) {
        post_res.on('data', function (chunk) {
            response += chunk;
        });

        post_res.on('end', function(){
            console.log('Response: ', response);
            res.send(response);
        });
    }); 
    post_req.write(post_data);
    post_req.end();
});

async function generateSignature(body){
    let checksum = await checksum_lib2.generateSignature(JSON.stringify(body), config.PaytmConfig.key);
    return checksum;
}

async function initiateTransaction(params){
    try{
        var paytmParams = {};

        paytmParams.body = {
            "requestType"   : "Payment",
            "mid"           : config.PaytmConfig.mid,
            "websiteName"   : "WEBSTAGING",
            "orderId"       : params.orderId,
            "callbackUrl"   : "http://localhost:4000/callback",
            "txnAmount"     : {
                "value"     : params.amount,
                "currency"  : "INR",
            },
            "userInfo"      : {
                "custId"    : "CUST_001",
            },
        };

        /*
        * Generate checksum by parameters we have in body
        * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        */
            let checksum = await generateSignature(paytmParams.body);

            paytmParams.head = {
                "signature"    : checksum 
            };

            var post_data = JSON.stringify(paytmParams);

            var options = {

                /* for Staging */
                hostname: 'securegw-stage.paytm.in',

                /* for Production */
                // hostname: 'securegw.paytm.in',

                port: 443,
                path: `/theia/api/v1/initiateTransaction?mid=${config.PaytmConfig.mid}&orderId=${params.orderId}`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': post_data.length
                }
            };

            let urls = `https://${options.hostname}${options.path}`;     
            const yy = {
                method : 'POST',
                url : urls,
                headers : options.headers,
                data : paytmParams
            }

        //     var response = "";
            let response  = await request(yy);
            return response.data.body.txnToken;
        //     var post_req = https.request(options, function(post_res) {
        //         post_res.on('data', function (chunk) {
        //             response += chunk;
        //         });

        //         post_res.on('end', function(){
        //             console.log('Response: ', response);
        //             return JSON.parse(response).body.txnToken;
        //         });
        //     });

        //     post_req.write(post_data);
        //     post_req.end();
    }catch(err){
        console.log(err);
    }
}

app.post("/refundStatusApi", [parseUrl, parseJson], (req, res) => {

    var paytmParams = {};

    paytmParams.body = {
        "mid"          : config.PaytmConfig.mid,
        "orderId"      : req.body.orderId,
        "refId"        : req.body.refundId,
    };

    /*
    * Generate checksum by parameters we have in body
    * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
    */
    checksum_lib2.generateSignature(JSON.stringify(paytmParams.body), config.PaytmConfig.key).then(function(checksum){

        paytmParams.head = {
            "signature" : checksum
        };

        var post_data = JSON.stringify(paytmParams);

        var options = {

            /* for Staging */
            hostname: 'securegw-stage.paytm.in',

            /* for Production */
            // hostname: 'securegw.paytm.in',

            port: 443,
            path: '/v2/refund/status',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };

        var response = "";
        var post_req = https.request(options, function(post_res) {
            post_res.on('data', function (chunk) {
                response += chunk;
            });

            post_res.on('end', function(){
                console.log('Response: ', response);
                res.send(response);
            });
        });

        post_req.write(post_data);
        post_req.end();
    });
});

app.post("/refundApi", [parseUrl, parseJson], (req, res) => {

    var paytmParams = {};

    paytmParams.body = {
        "mid"          : config.PaytmConfig.mid,
        "txnType"      : "REFUND",
        "orderId"      : req.body.orderId,
        "txnId"        : req.body.txnId,
        "refId"        : 'REFID' + new Date().getTime(),
        "refundAmount" : req.body.refundAmount,
    };

    /*
    * Generate checksum by parameters we have in body
    * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
    */
    checksum_lib2.generateSignature(JSON.stringify(paytmParams.body), config.PaytmConfig.key).then(function(checksum){

        paytmParams.head = {
            "signature"  : checksum
        };

        var post_data = JSON.stringify(paytmParams);

        var options = {

            /* for Staging */
            hostname: 'securegw-stage.paytm.in',

            /* for Production */
            // hostname: 'securegw.paytm.in',

            port: 443,
            path: '/refund/apply',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };

        var response = "";
        var post_req = https.request(options, function(post_res) {
            post_res.on('data', function (chunk) {
                response += chunk;
            });

            post_res.on('end', function(){
                console.log('Response: ', response);
                res.send(response);
            });
        });

        post_req.write(post_data);
        post_req.end();
    });
});

app.post("/paynow2", [parseUrl, parseJson], (req, res) => {
    // Route for making payment
  
    var paymentDetails = {
      amount: req.body.amount,
      customerId: req.body.name,
      customerEmail: req.body.email,
      customerPhone: req.body.phone
  }
  if(!paymentDetails.amount || !paymentDetails.customerId || !paymentDetails.customerEmail || !paymentDetails.customerPhone) {
      res.status(400).send('Payment failed')
  } else {
      var params = {};
      params['MID'] = config.PaytmConfig.mid;
      params['WEBSITE'] = config.PaytmConfig.website;
      params['CHANNEL_ID'] = 'WEB';
      params['INDUSTRY_TYPE_ID'] = 'Retail';
      params['ORDER_ID'] = 'TEST_'  + new Date().getTime();
      params['CUST_ID'] = paymentDetails.customerId;
      params['TXN_AMOUNT'] = paymentDetails.amount;
      params['CALLBACK_URL'] = 'http://localhost:4000/callback';
      params['EMAIL'] = paymentDetails.customerEmail;
      params['MOBILE_NO'] = paymentDetails.customerPhone;
  
  
      checksum_lib.genchecksum(params, config.PaytmConfig.key, function (err, checksum) {
          var txn_url = "https://securegw-stage.paytm.in/theia/processTransaction"; // for staging
          // var txn_url = "https://securegw.paytm.in/theia/processTransaction"; // for production
  
          var form_fields = "";
          for (var x in params) {
              form_fields += "<input type='hidden' name='" + x + "' value='" + params[x] + "' >";
          }
          form_fields += "<input type='hidden' name='CHECKSUMHASH' value='" + checksum + "' >";
  
          res.writeHead(200, { 'Content-Type': 'text/html' });
          res.write('<html><head><title>Merchant Checkout Page</title></head><body><center><h1>Please do not refresh this page...</h1></center><form method="post" action="' + txn_url + '" name="f1">' + form_fields + '</form><script type="text/javascript">document.f1.submit();</script></body></html>');
          res.end();
      });
  }
  });

app.post("/getStatusApi", [parseUrl, parseJson], (req, res) => {

    /* initialize an object */
    var paytmParams = {};

    /* body parameters */
    paytmParams.body = {

        /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
        "mid" : config.PaytmConfig.mid,

        /* Enter your order id which needs to be check status for */
        "orderId" : req.body.orderId,
    };

    /**
    * Generate checksum by parameters we have in body
    * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
    */
   checksum_lib2.generateSignature(JSON.stringify(paytmParams.body), config.PaytmConfig.key).then(function(checksum){
        /* head parameters */
        paytmParams.head = {

            /* put generated checksum value here */
            "signature"	: checksum
        };

        /* prepare JSON string for request */
        var post_data = JSON.stringify(paytmParams);

        var options = {

            /* for Staging */
            hostname: 'securegw-stage.paytm.in',

            /* for Production */
            // hostname: 'securegw.paytm.in',

            port: 443,
            path: '/v3/order/status',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };

        // Set up the request
        var response = "";
        var post_req = https.request(options, function(post_res) {
            post_res.on('data', function (chunk) {
                response += chunk;
            });

            post_res.on('end', function(){
                res.send(response);
            });
        });

        // post the data
        post_req.write(post_data);
        post_req.end();
    });
});

app.post("/paynow", [parseUrl, parseJson], (req, res) => {
    // Route for making payment

    var paymentDetails = {
        amount: req.body.amount,
        customerId: req.body.name,
        customerEmail: req.body.email,
        customerPhone: req.body.phone
    }
    if (false) {
        res.status(400).send('Payment failed')
    } else {
        var params = {};
        params['MID'] = config.PaytmConfig.mid;
        params['WEBSITE'] = config.PaytmConfig.website;
        params['CHANNEL_ID'] = 'WEB';
        params['INDUSTRY_TYPE_ID'] = 'Retail';
        params['ORDER_ID'] = 'TEST_' + new Date().getTime();
        params['CUST_ID'] = paymentDetails.customerId;
        params['TXN_AMOUNT'] = paymentDetails.amount;
        params['CALLBACK_URL'] = 'http://localhost:4000/callback';
        params['EMAIL'] = paymentDetails.customerEmail;
        params['MOBILE_NO'] = paymentDetails.customerPhone;

        var paytmParams = {};

        paytmParams.body = {
            "requestType": "Payment",
            "mid": config.PaytmConfig.mid,
            "websiteName": "WEBSTAGING",
            "orderId": 'TEST_' + new Date().getTime(),
            "txnAmount": {
                "value": paymentDetails.amount,
                "currency": "INR"
            },
            "userInfo": {
                "custId": paymentDetails.customerId
            },
            "callbackUrl": "http://localhost:4000/callback"
        };


        checksum_lib2.generateSignature(JSON.stringify(paytmParams.body), config.PaytmConfig.key).then(function(checksum){

                paytmParams.head = {
                    "signature"    : checksum
                };
            
                var post_data = JSON.stringify(paytmParams);
            
                var options = {
            
                    /* for Staging */
                    hostname: 'securegw-stage.paytm.in',
            
                    /* for Production */
                    // hostname: 'securegw.paytm.in',
            
                    port: 443,
                    path: `/theia/api/v1/initiateTransaction?mid=${config.PaytmConfig.mid}&orderId=${paytmParams.body.orderId}`,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Content-Length': post_data.length
                    }
                };
            
                var response = "";
                var post_req = https.request(options, function(post_res) {
                    post_res.on('data', function (chunk) {
                        response += chunk;
                    });
            
                    post_res.on('end', function(){
                        console.log(response);
                        res.send(response)
                    });
                });
            
                post_req.write(post_data);
                post_req.end();
            });
    }
});

app.post("/callback", (req, res) => {
    // Route for verifiying payment

    var body = '';

    req.on('data', function (data) {
        body += data;
    });

    req.on('end', function () {
        var html = "";
        var post_data = qs.parse(body);

        // received params in callback
        console.log('Callback Response: ', post_data, "\n");


        // verify the checksum
        var checksumhash = post_data.CHECKSUMHASH;
        // delete post_data.CHECKSUMHASH;
        var result = checksum_lib.verifychecksum(post_data, config.PaytmConfig.key, checksumhash);
        console.log("Checksum Result => ", result, "\n");


        // Send Server-to-Server request to verify Order Status
        var params = {
            "MID": config.PaytmConfig.mid,
            "ORDERID": post_data.ORDERID
        };

        checksum_lib.genchecksum(params, config.PaytmConfig.key, function (err, checksum) {

            params.CHECKSUMHASH = checksum;
            post_data = 'JsonData=' + JSON.stringify(params);

            var options = {
                hostname: 'securegw-stage.paytm.in', // for staging
                // hostname: 'securegw.paytm.in', // for production
                port: 443,
                path: '/merchant-status/getTxnStatus',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': post_data.length
                }
            };


            // Set up the request
            var response = "";
            var post_req = https.request(options, function (post_res) {
                post_res.on('data', function (chunk) {
                    response += chunk;
                });

                post_res.on('end', function () {
                    console.log('S2S Response: ', response, "\n");

                    var _result = JSON.parse(response);
                    if (_result.STATUS == 'TXN_SUCCESS') {
                        res.send('payment sucess')
                    } else {
                        res.send('payment failed')
                    }
                });
            });

            // post the data
            post_req.write(post_data);
            post_req.end();
        });
    });
});


app.listen(PORT, () => {
    console.log(`App is listening on Port ${PORT}`);
});